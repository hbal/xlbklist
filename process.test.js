import { test, expect, beforeEach } from 'vitest';
const { JSDOM } = require('jsdom');
import { fileURLToPath } from 'url';
import path from 'path';
import fs from 'fs';
const __dirname = path.resolve();
let dom;
beforeEach(async () => {
    const htmlPath = path.join(__dirname, 'index.html');
    dom = await JSDOM.fromFile(htmlPath, {
        runScripts: 'dangerously',
        resources: 'usable'
    });
    // Wait until the external scripts are loaded
    await new Promise((resolve) => {
        dom.window.document.addEventListener('DOMContentLoaded', () => {
            resolve();
        });
    });
    const mainScriptPath = path.join(__dirname, 'process.js');
    const mainScriptContent = fs.readFileSync(mainScriptPath, 'utf8');
    dom.window.eval(mainScriptContent);
    global.window = dom.window;
    global.document = dom.window.document;
    // Set all properties of the dom.window object as globals
    Object.keys(dom.window).forEach((key) => {
        if (!(key in global)) {
        global[key] = dom.window[key];
        }
    });
});

///// TESTS ///////////////////////////////////////////////

test('str2time', () => {
    expect(str2time("14:15")).toBe(14.25)
    expect(str2time("14:15:12")).toBe(14.25)
})

test('guessDateFormat',()=>{
    expect(guessDateFormat(["12.01.2023","23.01.2023","15.05.2023","30.03.2023","23.01.2023"])).toBe("DD.MM.YYYY")
    expect(guessDateFormat(["12-01-2023","23-01-2023","15-05-2023","30-03-2023","23-01-2023"])).toBe("DD-MM-YYYY")
    expect(guessDateFormat(["01/12/2023","01/23/2023","05/12/2023","03/23/2023","01/23/2023"])).toBe("MM/DD/YYYY")
});

test('cellbyStr',()=>{
    let ws=new Object({"A1":{v:1},"C3":{v:4},"D5":{},"AA2":{v:7}});
    expect(cellbyStr(ws,"A1")).toBe(1)
    expect(cellbyStr(ws,"AA2")).toBe(7)
    expect(cellbyStr(ws,"C3")).toBe(4)
    expect(cellbyStr(ws,"D5")).toBe("")
    expect(cellbyStr(ws,"A4")).toBe("")
});

test('cellbyNo',()=>{
    let ws=new Object({"A1":{v:1},"C3":{v:4},"D5":{},"AA2":{v:7}});
    expect(cellbyNo(ws,0,0)).toBe(1)
    expect(cellbyNo(ws,1,26)).toBe(7)
    expect(cellbyNo(ws,2,2)).toBe(4)
    expect(cellbyNo(ws,4,3)).toBe("")
    expect(cellbyNo(ws,3,0)).toBe("")
});

test('getColByCode',()=>{
    let ws=new Object({"A1":{v:1},"A2":{v:2},"C3":{v:4},"D5":{},"AA2":{v:7}});
    expect(getColByCode(ws,"A")).toEqual([1,2])
    expect(getColByCode(ws,"C")).toEqual([4])
    expect(getColByCode(ws,"D")).toEqual([""])
    expect(getColByCode(ws,"AA")).toEqual([7])
    expect(getColByCode(ws,"E")).toEqual([])
});

test('getColByInd',()=>{
    let ws=new Object({"A1":{v:1},"A2":{v:2},"C3":{v:4},"D5":{},"AA2":{v:7}});
    expect(getColByInd(ws,0)).toEqual([1,2])
    expect(getColByInd(ws,2)).toEqual([4])
    expect(getColByInd(ws,3)).toEqual([""])
    expect(getColByInd(ws,26)).toEqual([7])
    expect(getColByInd(ws,4)).toEqual([])
});