// generate code for opening local excel file
dayjs.extend(window.dayjs_plugin_customParseFormat)

// adds options of time intervals to the select element with id
// id: id of the select element
// mintime, maxtime: start and end time 14:00 formatted both inclusive
// increments: increment between the options in minutes
// seloption: selected options in the 14:00 format
function fillTimes(id, mintime, maxtime, increments, seloption) {
    let el = document.getElementById(id);
    let mint = str2time(mintime);
    let maxt = str2time(maxtime);
    let inc = increments / 60.;
    // console.log(mint)
    // console.log(maxt)
    // console.log(inc)
    // console.log(sel)
    for (let i = mint; i <= maxt; i += inc)
        el.appendChild(new Option(time2str(i), time2str(i)));
    el.value = seloption;
}

// returns a file url blob for a file object
// id: id of the file element
function getfileURLlocalfile(elementid) {
    let file = document.getElementById(elementid).files[0];
    var fileURL = URL.createObjectURL(file);
    return fileURL;
}

// define helper functions
var enc = XLSX.utils.encode_cell;
var cellbyStr = (ws, cellstr) => ws[cellstr]?.v?? ""
var cellbyNo = (ws, row, col) => ws[enc({ 'r': row, 'c': col })]?.v??""
// var cellbyNo = (ws, row, col) => (enc({ 'r': row, 'c': col }) in ws ? ((ws[enc({ 'r': row, 'c': col })].v)??"") : "")
var getColByCode = (ws, code) => Object.getOwnPropertyNames(ws).filter(x => (x.match(/[A-Z]+/)?.[0]??"") == code).map(x => ws[x]?.v??"")
var getColByInd = (ws, ind) => Object.getOwnPropertyNames(ws).filter(x => (x.match(/[A-Z]+/)?.[0]??"") == XLSX.utils.encode_col(ind)).map(x => ws[x]?.v??"")
var str2date = (dt, format = "") => {
    if (!dt) return "";
    if (dt instanceof Date) return dt;
    if (format == "") {
        let x = dt.split(".");
        return x.length == 3 ? new Date(x[2], x[1] - 1, x[0]) : ""
    }
    return dayjs(dt, format).$d
}
var dbdateformat = ""
var tatildateformat = ""
var guessDateFormat = (dt) => {
    if (!dt) return "";
    //console.log(dt)
    if (dt instanceof Date) return "";
    let guesses = ["YYYY-MM-DD", "DD-MM-YYYY", "DD/MM/YYYY", "MM/DD/YYYY", "DD.MM.YYYY"]
    let guess = guesses.map(f => dt.map(x => dayjs(x, f, true).isValid()).filter(x => x).length).reduce((prev, cur, ind) => cur > prev[0] ? [cur, ind] : prev, [null, ""])[1]
    if (guess != "") return guesses[guess];
    return ""
}
var str2time = (dt) => { let x = dt.split(":"); return x.length >= 2 ? parseFloat(x[0]) + parseFloat(x[1]) / 60. : ""; }
var time2str = (x) => { let hr = Math.floor(x); let min = Math.floor((x - hr) * 60); return hr.toString().padStart(2, "0") + ":" + min.toString().padStart(2, "0") }
var time2xltime = (x) => x/24;

function init(){
    // fill the options in col_adsoyad, col_bolum, col_tsure, col_tarih, col_giris, col_cikis select item options
    for (let selectid of [["col_adsoyad", "C"], ["col_bolum", "H"], ["col_tsure", "M"], ["col_tarih", "O"], ["col_giris", "P"], ["col_cikis", "R"]])
        for (let i = 65; i <= 90; i++) {
            let opt = new Option(String.fromCharCode(i), String.fromCharCode(i));
            document.getElementById(selectid[0]).appendChild(opt)
            if (String.fromCharCode(i) == selectid[1])
                document.getElementById(selectid[0]).value = selectid[1];
        }
    // fill izinyuzde and startrow select item options
    for (let i = 0; i <= 100; i += 5)
        document.getElementById("izinyuzde").appendChild(new Option(i, i))
    document.getElementById("izinyuzde").value = 20;
    for (let i = 1; i <= 10; i++)
        document.getElementById("startrow").appendChild(new Option(i, i))
    document.getElementById("startrow").value = 5;
    fillTimes("surekesim", "00:00", "08:00", 30, "05:00");
    fillTimes("giris", "08:00", "12:00", 30, "09:30");
    fillTimes("cikis", "12:00", "18:00", 30, "16:00");
}

// move progress bar with id of progressbarid to pct (float)
function setprogress(progressbarid,pct) {
    let progressbar = document.getElementById(progressbarid);
    pct = Math.floor(pct);
    progressbar.setAttribute('aria-valuenow', pct);
    progressbar.setAttribute('style', 'width:' + pct + '%');
    progressbar.textContent = pct + "%";
}

// process the input
async function process() {
    try {
        document.getElementById("process").disabled = "disabled";
        document.getElementById("message").innerHTML = "";
        document.getElementById("message").appendChild(document.getElementById("progress"))
        $('#resultModal').on("hide.bs.modal",()=>{
            document.getElementById("progresswrapper").appendChild(document.getElementById("progress"))
            $('#resultModal').off("hidden-bs-modal")
        })
        $('#resultModal').modal("show")
        setprogress("progressbar",0);
        // read parameters
        let startrow = parseFloat(document.getElementById("startrow").value)-1; // since indxing starts from 0 in XLSX
        let col_adsoyad = document.getElementById("col_adsoyad").value//"C";
        let col_bolum = document.getElementById("col_bolum").value//"H";
        let col_tsure = document.getElementById("col_tsure").value//"M";
        let col_tarih = document.getElementById("col_tarih").value//"O";
        let col_giris = document.getElementById("col_giris").value//"P";
        let col_cikis = document.getElementById("col_cikis").value//"R";
        let surekesim = document.getElementById("surekesim").value.split(":");
        surekesim = parseFloat(surekesim[0]) + parseFloat(surekesim[1]) / 60.;
        let giris = document.getElementById("giris").value.split(":");
        giris = parseFloat(giris[0]) + parseFloat(giris[1]) / 60.;
        let cikis = document.getElementById("cikis").value.split(":");
        cikis = parseFloat(cikis[0]) + parseFloat(cikis[1]) / 60.;
        let izinyuzde = parseFloat(document.getElementById("izinyuzde").value) / 100;
        // initialize variables
        let holidays = [];
        let namestolist = [];
        let leaves = {}; //obj of {name,[leavedates]}
        // load excel file
        let file = await fetch(getfileURLlocalfile("fileid"))
        file = await file.arrayBuffer();
        let workbook = XLSX.read(file);
        let wsin = workbook.Sheets[workbook.SheetNames[0]];
        let wsin2;
        if (workbook.SheetNames.length > 1 && workbook.SheetNames.includes("İzinler")) {
            wsin2 = workbook.Sheets["İzinler"];
            izindateformat = guessDateFormat(getColByInd(wsin2, 1))
            wsin2['!merges'] = [];
            i = 0
            tmp = []
            while (cellbyNo(wsin2, i, 1) != "") {
                let dt = str2date(cellbyNo(wsin2, i, 1).trim(), izindateformat);
                if (cellbyNo(wsin2, i, 1).trim() == "Tatil")
                    holidays.push(dt)
                tmp.push([cellbyNo(wsin2, i, 0).trim(), dt]);
                i++;
            }
            // console.log(tmp)
            let names = new Set(tmp.map(x => x[0]))
            names = Array.from(names)
            // console.log(names)
            names.forEach(name => leaves[name] = tmp.filter(l => l[0] == name).map(x => x[1]))
        }
        let wsin3;
        if (workbook.SheetNames.length > 1 && workbook.SheetNames.includes("Tatiller")) {
            wsin3 = workbook.Sheets["Tatiller"];
            tatildateformat = guessDateFormat(getColByInd(wsin3, 0))
            // console.log(tatildateformat)
            wsin3['!merges'] = [];
            i = 0
            tmp = []
            while (cellbyNo(wsin3, i, 0) != "") {
                let dt = str2date(cellbyNo(wsin3, i, 0).trim(), tatildateformat);
                holidays.push(dt)
                i++;
            }
        }
        // console.log("Holidays "+holidays)

        wsin['!merges'] = [];
        let endrow = startrow;
        while (cellbyStr(wsin, col_adsoyad + endrow).trim() != "")
            endrow++;
        endrow--;
        let endcol = 1;
        while (cellbyNo(wsin, startrow, endcol).trim() != "" || cellbyNo(wsin, startrow, endcol + 1).trim() != "" || cellbyNo(wsin, startrow, endcol + 2).trim() != "")
            endcol++;
        endcol--;
        let curadsoyad = ""
        let data = [];
        dbdateformat = guessDateFormat(getColByCode(wsin, col_tarih))
        setprogress("progressbar",40);
        for (let crow = startrow; crow < endrow; crow++) { // not endrow-1 since sheetjs index starts from 0
            let pres;
            let day;
            let curadsoyad = cellbyStr(wsin, col_adsoyad + (crow + 1)).trim()
            let cbolum = cellbyStr(wsin, col_bolum + (crow + 1)).trim()
            let ctsure = str2time(cellbyStr(wsin, col_tsure + (crow + 1)).trim())
            let ctarih = str2date(cellbyStr(wsin, col_tarih + (crow + 1)).trim(), format = dbdateformat)
            let cgiris = str2time(cellbyStr(wsin, col_giris + (crow + 1)).trim())
            let ccikis = str2time(cellbyStr(wsin, col_cikis + (crow + 1)).trim())
            // console.log(crow+" t:"+col_tarih+" "+ctarih)
            if (namestolist && !curadsoyad in namestolist) {
                continue;
            }
            if (cgiris != 0 && ccikis != 0 && ccikis >= cikis && cgiris <= giris) pres = 4; // gelmiş ve giriş çıkış tamam
            else if (cgiris != 0 && ccikis != 0 && ctsure < surekesim) pres = 2; // gelmiş ama süre az
            else if (cgiris != 0 && ccikis != 0 && ctsure >= surekesim) pres = 3; // gelmiş ve süre tamam
            else if (cgiris == "" && ccikis == "") pres = 0; // gelmemiş
            else pres = 1; // gelmiş ama giriş-çıkıştan biri yok
            // console.log(ctarih)
            if (typeof ctarih.getDay === 'function' && [0, 6].includes(ctarih.getDay())) day = 0; // hsonu
            else if (holidays.map(x => x.getTime()).includes(ctarih.getTime())) day = 1; // tatil
            else if (Object.keys(leaves).includes(curadsoyad) && leaves[curadsoyad].map(x => x.getTime()).includes(ctarih.getTime())) day = 2; // izin
            else day = 3; // işgünü
            // console.log("curadsoyad",curadsoyad)
            // console.log("leaves",leaves)
            // console.log("ctarih",ctarih)
            data.push([curadsoyad, pres, day, cbolum, ctsure, ctarih, cgiris, ccikis]);
            setprogress("progressbar",40 + 30 * (crow - startrow + 1) / (endrow - startrow + 1));
        }
        let presnames = { 0: "Gelmemiş", 1: "Gelmiş (Giriş veya çıkış eksik)", 2: "Gelmiş (Süre eksik)", 3: "Gelmiş (Süre tamam)", 4: "Gelmiş (Giriş çıkış tamam)" }
        let daynames = { 0: "Haftasonu", 1: "Tatil", 2: "İzin verilmiş", 3: "İşgünü" }
        data = data.toSorted((x, y) => x[0] >= y[0])
        // console.log("data:",data)
        // console.log("leaves",leaves)

        data = data.map(x => {
            if (x[2] == 3 && x[1] == 0) return [0, ...x]; // gelmediği işgünleri
            if (x[2] == 3 && [1, 2].includes(x[1])) return [1, ...x]; // geldiği süresi az veya giriş/çıkıştan biri olmayan işgünleri
            if (x[2] == 3 && x[1] == 3) return [2, ...x]; // geldiği süre ok işgünleri
            if (x[2] == 3 && x[1] == 4) return [3, ...x]; // geldiği giriş çıkış ok işgünleri
            if (x[2] in [0, 1, 2] && [1, 2].includes(x[1])) return [4, ...x]; // giriş veya çıkış kaydı olan tatiller/izinler
            if (x[2] in [0, 1, 2] && [3, 4].includes(x[1])) return [5, ...x];// geldiği ve süreye uyan veya giriş çıkış ok tatiller/izinler
            if (x[2] in [0, 1, 2] && x[1] == 0) return [6, ...x];// gelmediği tatiller
        });
        // console.log(data)
        let names = new Set(data.map(x => x[1]))
        // console.log(names)
        let out = [];
        for (let name of names) {
            let curdata = data.filter(x => x[1] === name)
            let totalwday = curdata.filter(x => x[3] == 3).length;
            out.push({
                "Ad Soyad": name,
                "Bölüm": curdata[0][4],
                "Gelmediği işgünleri": curdata.filter(x => x[0] == 0).length,
                "Geldiği ama süresi az veya girişi veya çıkışı eksik işgünleri": curdata.filter(x => x[0] == 1).length,
                "Geldiği ve süresi uyan işgünleri": curdata.filter(x => x[0] == 2).length,
                "Geldiği ve giriş ve çıkışı uyan iş günleri": curdata.filter(x => x[0] == 3).length,
                "Geldiği ve giriş veya çıkış kaydı olmayan tatiller": curdata.filter(x => x[0] == 4).length,
                "Geldiği ve süresi veya giriş çıkışı uyan tatiller": curdata.filter(x => x[0] == 5).length,
                "Akademik izin gün sayısı": Math.ceil(totalwday * izinyuzde)
            });
        }
        // console.log(out);
        let out2 = []
        for (let row of data) {
            out2.push({
                "Ad Soyad": row[1],
                "Bölüm": row[4],
                "Tarih": row[6],
                "Giriş": time2xltime(row[7]),
                "Çıkış": time2xltime(row[8]),
                "Süre (saat)": Math.round(row[5] * 100) / 100,
                "Gün": daynames[row[3]],
                "Durum": presnames[row[2]]
            });
        }
        setprogress("progressbar",70);
        const wsout1 = XLSX.utils.json_to_sheet(out);
        wsout1["!cols"] = [];
        wsout1["!cols"][0] = { wch: 30 };
        wsout1["!cols"][1] = { wch: 45 };
        wsout1["!cols"][2] = { wch: 10 };
        wsout1["!cols"][3] = { wch: 10 };
        wsout1["!cols"][4] = { wch: 10 };
        wsout1["!cols"][5] = { wch: 10 };
        wsout1["!cols"][6] = { wch: 10 };
        wsout1["!cols"][7] = { wch: 10 };
        wsout1["!cols"][7] = { wch: 10 };
        const wsout2 = XLSX.utils.json_to_sheet(out2);
        for(let col of [3,4]){
            for(let row=1;row<data.length;row++){
                if(enc({ 'r': row, 'c': col }) in wsout2) wsout2[enc({ 'r': row, 'c': col })]["z"]="hh:mm"
            }
        }
        for(let col of [3,4]){
            for(let row=1;row<data.length;row++){
                if(enc({ 'r': row, 'c': col }) in wsout2 && wsout2[enc({ 'r': row, 'c': col })].v==0) {
                    wsout2[enc({ 'r': row, 'c': col })]["t"]="s";
                    wsout2[enc({ 'r': row, 'c': col })]["v"]="";
                }
            }
        }
        wsout2["!cols"] = [];
        wsout2["!cols"][0] = { wch: 30 };
        wsout2["!cols"][1] = { wch: 45 };
        wsout2["!cols"][2] = { wch: 12 };
        wsout2["!cols"][3] = { wch: 6 };
        wsout2["!cols"][4] = { wch: 6 };
        wsout2["!cols"][5] = { wch: 6 };
        wsout2["!cols"][6] = { wch: 10 };
        wsout2["!cols"][7] = { wch: 30 };
        const wbout = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wbout, wsout1, "Özet");
        XLSX.utils.book_append_sheet(wbout, wsout2, "Detay");
        XLSX.writeFile(wbout, "liste.xlsx", { compression: true});
        setprogress("progressbar",100);
        document.getElementById("process").disabled = false;
        document.getElementById("message").insertAdjacentHTML("beforeend","<div style='text-align:center;width=100%'>Dosya indirilmiştir.</div>");
    } catch (err) {
        var aux = err.stack.split("\n");
        document.getElementById("message").innerText = "Hata var:\n" + aux.join('\n"')+err.message;
        $('#resultModal').modal("show")
    }
}
//process();

document.addEventListener("DOMContentLoaded", (event) => {
    init();
  });